package com.example.rxjava

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.rxjava.data.Person
import com.example.rxjava.network.Client
import com.example.rxjava.network.PersonConverter
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_database.*
import java.util.*

class DatabaseActivity : AppCompatActivity() {

    private var compositeDisposable = CompositeDisposable() // can be collected in one "bucket"
    private var networkAndDB: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_database)
        insertButton.setOnClickListener {
            insertToDb()
        }

        networkButton.setOnClickListener {
            getFromNetwork()
        }

        networkDbButton.setOnClickListener {
            getFromNetworkAndInsertToDb()
        }
    }

    override fun onDestroy() {
        networkAndDB?.dispose()
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun getFromNetworkAndInsertToDb() {
        networkAndDB =
            Client.getService().getPersons()
                .map { list -> list.persons.map { PersonConverter.toDatabase(it) } }
                // or if you want to return the list, use .flatMap { Single.fromCallable { db... } }
                .flatMapCompletable { list ->
                    Completable.fromCallable {
                        val db = (applicationContext as App).db
                        db.runInTransaction {
                            db.personDao().insertAll(list)
                        }
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressBar.isVisible = true }
                .doFinally { progressBar.isVisible = false }
                .subscribe(
                    {
                        Toast.makeText(this, "Inserted", Toast.LENGTH_SHORT).show()
                    },
                    {
                        Log.e("App", "Error occurred", it)
                        Toast.makeText(this, "Error occurred", Toast.LENGTH_SHORT).show()
                    }
                )
    }

    private fun getFromNetwork() {
        val disposable =
            Client.getService().getPersons()
                .map { response ->
                    response.persons.map { person ->
                        PersonConverter.toDatabase(person)
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressBar.isVisible = true }
                .doFinally { progressBar.isVisible = false }
                .subscribe(
                    { persons ->
                        Log.d("App", "Persons $persons")
                        Toast.makeText(this, "Got ${persons.size} persons", Toast.LENGTH_SHORT)
                            .show()
                    },
                    {
                        Log.e("App", "Error occurred", it)
                        Toast.makeText(this, "Error occurred", Toast.LENGTH_SHORT).show()
                    }
                )
        compositeDisposable.add(disposable)
    }

    private fun insertToDb() {
        val disposable = Completable.fromAction {
            val db = (applicationContext as App).db
            db.runInTransaction {
                db.personDao().insertAll(
                    listOf(
                        Person(1, "John Doe", Calendar.getInstance()),
                        Person(2, "Alex MC", Calendar.getInstance()),
                        Person(3, "Sean Murphy", Calendar.getInstance()),
                        Person(4, "Bob Smith", Calendar.getInstance()),
                        Person(5, "Donald Trump", Calendar.getInstance())
                    )
                )
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete { /* do something */ }
            .doOnDispose { /* do something */ }
            .doOnError { /* do something */ }
            .subscribe(
                {
                    Toast.makeText(this, "Inserted", Toast.LENGTH_SHORT).show()
                },
                {
                    Log.e("App", "Error occurred", it)
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }
            )
        compositeDisposable.add(disposable)
    }
}