package com.example.rxjava

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.rxjava.network.Client
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_ui.*
import java.util.*
import java.util.concurrent.TimeUnit

class UiActivity : AppCompatActivity(R.layout.activity_ui) {

    private var subject: PublishSubject<String>? = null
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        subject = PublishSubject.create()

        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                subject?.onNext(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        disposable = subject
            ?.debounce(250, TimeUnit.MILLISECONDS)
            ?.map { it.trim().toLowerCase(Locale.getDefault()) }
            ?.switchMapSingle { query: String ->
                // go to network and fetch data...
                Client.getService().search(query)
            }
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ query ->
                Log.d("App", "query: $query")
            }, {
                Log.e("App", "query error", it)
            })
    }

    override fun onDestroy() {
        disposable?.dispose()
        subject?.onComplete()
        super.onDestroy()
    }
}