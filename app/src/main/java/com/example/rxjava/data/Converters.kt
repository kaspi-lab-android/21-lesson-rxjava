package com.example.rxjava.data

import androidx.room.TypeConverter
import java.util.*

/**
 * Конвертер типов данных позволяет Room ссылаться на сложные типы данных.
 */
class Converters {
    @TypeConverter
    fun calendarToTimestamp(calendar: Calendar): Long = calendar.timeInMillis

    @TypeConverter
    fun timestampToCalendar(value: Long): Calendar =
        Calendar.getInstance().apply { timeInMillis = value }
}