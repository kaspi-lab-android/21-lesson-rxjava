package com.example.rxjava.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.rxjava.data.Person.Companion.TABLE_NAME
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = TABLE_NAME)
data class Person(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_PERSON_ID)
    val personId: Int,
    @ColumnInfo(name = COLUMN_FULLNAME)
    val fullname: String?,
    @ColumnInfo(name = COLUMN_CREATED_DATE)
    val createdDate: Calendar = Calendar.getInstance()
) {

    override fun toString(): String =
        """
            Person(
                id=$personId,
                fullname=$fullname,
                createdDate=${SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(createdDate.time)}
            )
        """.trimIndent()

    companion object {
        const val TABLE_NAME = "persons"
        const val COLUMN_PERSON_ID = "person_id"
        const val COLUMN_FULLNAME = "full_name"
        const val COLUMN_CREATED_DATE = "created_date"
    }
}