package com.example.rxjava.data

import androidx.room.*
import com.example.rxjava.data.Person.Companion.COLUMN_FULLNAME
import com.example.rxjava.data.Person.Companion.COLUMN_PERSON_ID
import com.example.rxjava.data.Person.Companion.TABLE_NAME

@Dao
interface PersonDao {
    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll(): List<Person>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(persons: List<Person>)

    @Delete
    fun delete(person: Person)

    @Query("SELECT * FROM $TABLE_NAME WHERE $COLUMN_PERSON_ID IN (:personIds)")
    fun loadAllByIds(personIds: IntArray): List<Person>

    @Query("""SELECT * FROM $TABLE_NAME WHERE $COLUMN_FULLNAME LIKE :first 
             LIMIT 1""")
    fun findByName(first: String): Person
}