package com.example.rxjava

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbButton.setOnClickListener {
            startActivity(Intent(this, DatabaseActivity::class.java))
        }

        uiButton.setOnClickListener {
            startActivity(Intent(this, UiActivity::class.java))
        }
    }
}