package com.example.rxjava

import android.app.Application
import com.example.rxjava.data.MyDatabase

class App : Application() {

    lateinit var db: MyDatabase
        private set

    override fun onCreate() {
        super.onCreate()
        db = MyDatabase.getInstance(this)
    }
}