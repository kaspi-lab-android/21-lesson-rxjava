package com.example.rxjava.network

import com.example.rxjava.network.response.PersonsResponse
import com.example.rxjava.network.response.SearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MyService {

    @GET("v3/14ad1d03-abb1-432c-98a8-d3ea8e9f6254")
    fun getPersons(): Single<PersonsResponse>

    @GET("v3/8ebd5d7c-7e6a-4f88-b14f-3d926101cddd")
    fun search(
        @Query("q") query: String
    ): Single<SearchResponse>
}