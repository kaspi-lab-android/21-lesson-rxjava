package com.example.rxjava.network.response

import com.google.gson.annotations.SerializedName

data class PersonsResponse(
    val persons: List<PersonData>
)

data class PersonData(
    val id: Int,
    @SerializedName("name")
    val fullname: String,
    val created: Long
)