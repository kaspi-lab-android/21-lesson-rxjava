package com.example.rxjava.network

import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Client {

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: MyService? = null

        fun getService(): MyService {
            return instance ?: synchronized(this) {
                instance ?: Client().build()
            }
        }
    }

    fun build(): MyService =
        Retrofit.Builder()
            .baseUrl("https://run.mocky.io/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(buildClient(getDefaultInterceptor()))
            .build()
            .create(MyService::class.java)

    private fun buildClient(interceptor: Interceptor?): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .apply {
                if (interceptor != null) {
                    addInterceptor(interceptor)
                }
            }
            .build()

    private fun getDefaultInterceptor(): Interceptor =
        Interceptor { chain ->
            chain.proceed(
                chain.request()
                    .newBuilder()
                    .addHeader("User-Agent", "Android phone")
                    .addHeader("key", "some data")
                    .build()
            )
        }
}