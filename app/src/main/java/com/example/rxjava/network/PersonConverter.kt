package com.example.rxjava.network

import com.example.rxjava.data.Person
import com.example.rxjava.network.response.PersonData
import java.util.*

object PersonConverter {

    fun toDatabase(person: PersonData) =
        Person(
            person.id,
            person.fullname,
            Calendar.getInstance().apply { timeInMillis = person.created }
        )
}