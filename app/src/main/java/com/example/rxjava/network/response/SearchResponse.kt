package com.example.rxjava.network.response

data class SearchResponse(
    val result: List<String>
)